package Programmingnew;

import javax.swing.*;
import java.awt.*;

public class Menue {

    private int n; //колво кнопок
    private Color color1; //Цвет
    private double x;//координата х кнопки
    private double y;//координата Y кнопки
    private double w;//ширина кнопки
    private double h;//высота кнопки

    private String img; //путь к картинке кнопки

    String[] list = new String[5];//список из 5-ти элементов типа строка - названия кнопок

    public Menue(){
        x=100;
        y = 0;
        w = 300;
        h = 150;
        n = 5;
        color1 = Color.BLACK;
        img = "res/but1.png";


        list[0] = "Новый игрок";
        list[1] = "Играть";
        list[2] = "Настройки";
        list[3] = "Правила";
        list[4] = "Выход";

    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getW() {
        return w;
    }

    public double getH() {
        return h;
    }

    public void update(){

    }
    public void draw(Graphics2D g){
        for (int i = 0; i < n; i++) {
            g.drawImage(new ImageIcon(img).getImage(),(int)x,(int)(y+140)*i, null);

            g.setColor(color1);//задаем цвет обьекту
            Font font = new Font("Arial",Font.ITALIC,45);
            g.setFont(font);

            long length = (int) g.getFontMetrics().getStringBounds(list[i],g).getWidth();
            g.drawString(list[i],(int)(x+w/2)-(int)(length/2),(int)((y+140)*i+(h/3)*2));


        }
    }




}
