package Programmingnew;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.nio.Buffer;

public class Panel extends JPanel implements ActionListener{
    public static int WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;
    public static int HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height;

    public static int mouseX;
    public static int mouseY;

    Timer mainTimer = new Timer(30, this);
    public static enum STATES{MENUE,PLAY}
    public static STATES state = STATES.MENUE;

    public BufferedImage image;
    private Graphics2D g;

    Back back = new Back();
    Player player = new Player();
    Menue menue = new Menue();

    public Panel() {
        addMouseListener(new Listners());
        addKeyListener(new Listners());
        addMouseMotionListener(new Listners());
        setFocusable(true);
        requestFocus();
        mainTimer.start();
        image = new BufferedImage(WIDTH,HEIGHT,BufferedImage.TYPE_INT_RGB);
        g = (Graphics2D)image.getGraphics();
    }


    public void actionPerformed(ActionEvent e){
        if(state.equals(STATES.MENUE)){
            back.draw(g);
            menue.draw(g);
        if (Panel.mouseX >menue.getX()&& Panel.mouseY<menue.getY()+menue.getW()
                &&Panel.mouseY>(menue.getY()+140)*0&&Panel.mouseY<(menue.getY()+140)*0+menue.getH()) {//Если курсор попал в кнопку
        menue.list[0] = "NEw user"
        }else {
            menue.list[0] = String.valueOf("Новый игрок");
        }


            gameDraw();

        }
        if (state.equals(STATES.PLAY)){
            gameUpdate();
            gameRender();
            gameDraw();

        }

    }
    public void gameRender(){
        back.draw(g);
        player.draw(g);

    }
    public void gameUpdate(){
        player.update();
    }
    private void gameDraw(){
        Graphics g2 = this.getGraphics();
        g2.drawImage(image,0,0,null);
        g2.dispose();
    }
}
