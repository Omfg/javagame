package Programmingnew;

import javax.swing.*;
import java.awt.*;

public class Back {
    Image img = new ImageIcon("res/fon1.jpg").getImage();

    public void draw(Graphics2D g){
        Color bacColor = new Color(37,255,38);
        g.setColor(bacColor);
        if(Panel.state.equals(Panel.STATES.MENUE))g.fillRect(0,0,Panel.WIDTH,Panel.HEIGHT);
        if (Panel.state.equals(Panel.STATES.PLAY))g.drawImage(img,(int)0,(int)0,null);
    }
}
